package com.zaluskyi.store;

import com.zaluskyi.ingredientfactory.KyivPizzaIngredientFactory;
import com.zaluskyi.ingredientfactory.PizzaIngredientFactory;
import com.zaluskyi.pizza.*;

public class KyivPizzaStore extends PizzaStore {
    @Override
    Pizza createPizza(PizzaType type) {
        Pizza pizza = null;
        PizzaIngredientFactory ingredientFactory = new KyivPizzaIngredientFactory();
        if (type.equals(PizzaType.CHEESE)) {
            pizza = new CheesePizza(ingredientFactory);
            pizza.setName("Kyiv style cheese pizza");
        } else if (type.equals(PizzaType.CLAM)) {
            pizza = new ClamPizza(ingredientFactory);
            pizza.setName("Kyiv style calm pizza");
        } else if (type.equals(PizzaType.VEGGIE)) {
            pizza = new VeggiePizza(ingredientFactory);
            pizza.setName("Kyiv style veggie pizza");
        } else if (type.equals(PizzaType.PEPPERONI)) {
            pizza = new PepperoniPizza(ingredientFactory);
            pizza.setName("Kyiv style pepperoni pizza");
        }
        return pizza;
    }
}
