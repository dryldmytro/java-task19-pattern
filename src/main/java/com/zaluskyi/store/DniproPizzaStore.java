package com.zaluskyi.store;

import com.zaluskyi.ingredientfactory.LvivPizzaIngredientFactory;
import com.zaluskyi.ingredientfactory.PizzaIngredientFactory;
import com.zaluskyi.pizza.*;

public class DniproPizzaStore extends PizzaStore {
    @Override
    Pizza createPizza(PizzaType type) {

        Pizza pizza = null;

        PizzaIngredientFactory ingredientFactory = new LvivPizzaIngredientFactory();
        if (type.equals(PizzaType.CHEESE)) {
            pizza = new CheesePizza(ingredientFactory);
            pizza.setName("Dnipro style cheese pizza");
        } else if (type.equals(PizzaType.CLAM)) {
            pizza = new ClamPizza(ingredientFactory);
            pizza.setName("Dnipro style calm pizza");
        } else if (type.equals(PizzaType.VEGGIE)) {
            pizza = new VeggiePizza(ingredientFactory);
            pizza.setName("Dnipro style veggie pizza");
        } else if (type.equals(PizzaType.PEPPERONI)) {
            pizza = new PepperoniPizza(ingredientFactory);
            pizza.setName("Dnipro style pepperoni pizza");
        }
        return pizza;
    }
}
