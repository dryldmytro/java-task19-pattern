package com.zaluskyi.ingredientfactory;

import com.zaluskyi.ingredient.pepperoni.SweetPepperoni;
import com.zaluskyi.ingredient.vegies.Veggies;
import com.zaluskyi.ingredient.cheese.Cheese;
import com.zaluskyi.ingredient.clam.Clam;
import com.zaluskyi.ingredient.clam.FreshClam;
import com.zaluskyi.ingredient.dough.Dough;
import com.zaluskyi.ingredient.dough.ThickDough;
import com.zaluskyi.ingredient.pepperoni.Pepperoni;
import com.zaluskyi.ingredient.souce.BBQSause;
import com.zaluskyi.ingredient.souce.Sauce;

public class LvivPizzaIngredientFactory implements PizzaIngredientFactory {

    public Dough createDough() {
        return new ThickDough();
    }

    public Sauce createSauce() {
        return new BBQSause();
    }

    public Cheese createCheese() {
        return null;
    }

    public Veggies[] createVeggies() {
        return new Veggies[0];
    }

    public Pepperoni createPepperoni() {
        return new SweetPepperoni();
    }

    public Clam createClam() {
        return new FreshClam();
    }
}
