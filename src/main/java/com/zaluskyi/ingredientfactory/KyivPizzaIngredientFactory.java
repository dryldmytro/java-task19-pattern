package com.zaluskyi.ingredientfactory;

import com.zaluskyi.ingredient.cheese.Mozarella;
import com.zaluskyi.ingredient.clam.FrozenClam;
import com.zaluskyi.ingredient.pepperoni.SweetPepperoni;
import com.zaluskyi.ingredient.souce.TomatoSause;
import com.zaluskyi.ingredient.vegies.*;
import com.zaluskyi.ingredient.cheese.Cheese;
import com.zaluskyi.ingredient.clam.Clam;
import com.zaluskyi.ingredient.dough.Dough;
import com.zaluskyi.ingredient.dough.ThickDough;
import com.zaluskyi.ingredient.pepperoni.Pepperoni;
import com.zaluskyi.ingredient.souce.Sauce;

public class KyivPizzaIngredientFactory implements PizzaIngredientFactory {

    public Dough createDough() {
        return new ThickDough();
    }

    public Sauce createSauce() {
        return new TomatoSause();
    }

    public Cheese createCheese() {
        return new Mozarella();
    }

    public Veggies[] createVeggies() {
        return new Veggies[]{new Garlic(),new RedPepper(),new Mushroom()};
    }

    public Pepperoni createPepperoni() {
        return new SweetPepperoni();
    }

    public Clam createClam() {
        return new FrozenClam();
    }
}
