package com.zaluskyi.ingredientfactory;

import com.zaluskyi.ingredient.pepperoni.SpacyPepperoni;
import com.zaluskyi.ingredient.vegies.Mushroom;
import com.zaluskyi.ingredient.vegies.Onion;
import com.zaluskyi.ingredient.vegies.RedPepper;
import com.zaluskyi.ingredient.vegies.Veggies;
import com.zaluskyi.ingredient.cheese.Cheese;
import com.zaluskyi.ingredient.cheese.Maskarpone;
import com.zaluskyi.ingredient.clam.Clam;
import com.zaluskyi.ingredient.clam.FrozenClam;
import com.zaluskyi.ingredient.dough.Dough;
import com.zaluskyi.ingredient.dough.ThickDough;
import com.zaluskyi.ingredient.pepperoni.Pepperoni;
import com.zaluskyi.ingredient.souce.Sauce;
import com.zaluskyi.ingredient.souce.SpacySauce;

public class DniproPizzaIngredientFactory implements PizzaIngredientFactory {
    public Dough createDough() {
        return new ThickDough();
    }

    public Sauce createSauce() {
        return new SpacySauce();
    }

    public Cheese createCheese() {
        return new Maskarpone();
    }

    public Veggies[] createVeggies() {
        return new Veggies[]{new Onion(),new Mushroom(),new RedPepper()};
    }

    public Pepperoni createPepperoni() {
        return new SpacyPepperoni();
    }

    public Clam createClam() {
        return new FrozenClam();
    }
}
