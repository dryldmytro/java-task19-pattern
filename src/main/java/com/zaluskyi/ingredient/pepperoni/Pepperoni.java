package com.zaluskyi.ingredient.pepperoni;

public abstract class Pepperoni {

  public Pepperoni() {
    getName();
  }

  void getName(){
    System.out.println(getClass().getSimpleName());
  }
}
