package com.zaluskyi.ingredient.cheese;

public abstract class Cheese {

    public Cheese() {
        getName();
    }

    public  void getName(){
        System.out.println(getClass().getSimpleName());
    }
}
