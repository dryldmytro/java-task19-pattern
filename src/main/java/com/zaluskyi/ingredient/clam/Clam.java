package com.zaluskyi.ingredient.clam;

public abstract class Clam {

  public Clam() {
    getName();
  }

  void getName(){
    System.out.println(getClass().getSimpleName());
  }
}
